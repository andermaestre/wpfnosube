﻿Public Class VerFotosWindow
    Dim viewmodel As FotosViewModel

    Public Sub New(inm As Inmuebles, pos As Integer)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        viewmodel = New FotosViewModel(inm, pos)

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
        viewmodel.Retroceder()
    End Sub

    Private Sub Window_Loaded(sender As Object, e As RoutedEventArgs)
        Grid1.DataContext = viewmodel
    End Sub

    Private Sub Button_Click_1(sender As Object, e As RoutedEventArgs)

        viewmodel.Avanzar()
    End Sub
End Class
