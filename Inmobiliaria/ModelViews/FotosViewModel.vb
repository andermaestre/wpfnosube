﻿Imports System.ComponentModel

Public Class FotosViewModel
    Implements INotifyPropertyChanged
    Dim InmuebleActual As Inmuebles
    Dim pos As Integer
    Public Sub New(Inmueble As Inmuebles, posicion As Integer)
        ''Inicializa los miembros con los parámetros
        InmuebleActual = Inmueble
        pos = posicion
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("PosicionActual"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Foto"))
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("TotalFotos"))
    End Sub
    Public ReadOnly Property Foto As Byte()
        ''Devuelve la imagen del inmueble de la posición actual
        Get
            Return InmuebleActual.Imagenes(pos).Foto
        End Get
    End Property
    Public ReadOnly Property PosicionActual As Integer
        ''Devuelve la posición actual
        Get
            Return pos + 1
        End Get
    End Property
    Public ReadOnly Property TotalFotos As Integer
        ''Devuelve el contador de fotos para el inmueble actual
        Get
            Return InmuebleActual.Imagenes.Count
        End Get
    End Property
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Public Sub Avanzar()
        ''Si no estamos en la última foto del inmueble actual avanza a la 
        ''siguiente y notifica el cambio.
        If pos < InmuebleActual.Imagenes.Count - 1 Then
            pos += 1
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("PosicionActual"))
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Foto"))
        End If
    End Sub
    Public Sub Retroceder()
        ''Si no estamos en la última foto del inmueble actual retrocede
        ''a la anterior y notifica el cambio

        If pos > 0 Then
            pos -= 1
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("PosicionActual"))
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Foto"))
        End If
    End Sub

End Class
