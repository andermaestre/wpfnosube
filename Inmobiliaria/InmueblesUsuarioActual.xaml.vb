﻿Public Class InmueblesUsuarioActual

    Private Sub Window_Loaded(sender As Object, e As RoutedEventArgs)
        Grid1.DataContext = usuarioActual
    End Sub



    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
        Dim frm As New FotoWindow(inmuebleActual)
        frm.ShowDialog()
    End Sub



    Private Sub CmbInmuebles_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles CmbInmuebles.SelectionChanged
        inmuebleActual = CmbInmuebles.SelectedItem
    End Sub

    Private Sub Lb_MouseDoubleClick(sender As Object, e As MouseButtonEventArgs) Handles lb.MouseDoubleClick
        Dim f As New VerFotosWindow(CmbInmuebles.SelectedItem, lb.SelectedIndex)
        f.Show()
    End Sub
End Class
